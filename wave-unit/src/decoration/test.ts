import { UnitOptions } from "../abstraction";
import { units } from "../globalization";
import { Unit } from "../classification";

export function Test (options: UnitOptions){
    return (target: any, key: string, descriptor: PropertyDescriptor) => {
        let unit = new Unit(options);
        unit.test = target[key];
        units[unit.name] = unit;
    }
}