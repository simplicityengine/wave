export class Assertion {

    public equals(actual: string, expected: string, message: string);
    public equals(actual: number, expected: number, message: string);
    public equals(actual: boolean, expected: boolean, message: string);
    public equals(actual, expected, message: string) {
        let regex = /string|number|boolean|null|undefined/i;
        if(regex.test(typeof actual) && regex.test(typeof expected)){
            if(actual === expected){
                return;
            }
        }
        throw new Error(message);
    }


    public regex(expression: RegExp, content: string, message: string){
        if(typeof content === 'string' && expression instanceof RegExp){
            if(expression.test(content)){
                return;
            }
        }
        throw new Error(message);
    }
}