import { UnitOptions } from '../abstraction';
export class Unit implements UnitOptions{
    public type: new () => any;
    public method: string;
    public name: string;
    public test: Function;
    public executed: boolean = false;
    public success: boolean = false;
    public error: Error;

    public constructor(options?: UnitOptions){
        if(options){
            Object.assign(this, options);
        }

    }

    public execute(){
        try{
            this.executed = true;
            let test = this.test;
            if(typeof test === 'function')
                test();
            else
                throw new Error('Test Case Not Defined!');
        }
        catch(error){
            this.error = error;
            this.success = false;
        }
    }
    public get status(): string{
        return (this.success ? 'PASSED' : 'FAILED')
    }

    public toString(): string{
        let type = this.type;
        let success = this.success;
        let method = this.method;
        let status = this.status;
        let error = this.error;

        let message = (error ? error.message : (success ? 'unit test passed successfully' : 'unit test not executed'))
        let template = "[{status}] [{type}] [{method}] {message}";

        template = template.replace('{status}', status);
        template = template.replace('{type}', (type ? type.name : '*'));
        template = template.replace('{method}', (method ? method : '*'));
        template = template.replace('{message}', (method ? message : '*'));
        
        return template;
    }
}