export function randomName() {
    var text = "UNIT-";
    var possible = "0123456789ABCDEF";

    for (var i = 0; i < 3; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}