import { Unit } from "../classification";

export let units: { 
    executed: number, 
    passed: number, 
    failed: number, 
    defined: number,
    execute: () => void 
} & {[key: string]: Unit}  = <any>{};


['passed', 'failed', 'executed', 'defined' ].forEach((key) => {
    Object.defineProperty(units, key, {
        enumerable: false,
        get: () => {
            let count = 0;
            for(var k in units){
                let unit = units[k];
                switch(key){
                    case 'passed': (unit.executed && unit.success ? count++ : undefined); break;
                    case 'failed': (unit.executed && !unit.success ? count++ : undefined); break;
                    case 'executed': (unit.executed ? count++ : undefined); break;
                    case 'defined': count++; break;
                }
            }
            return count;
        }
    })
});

units.execute = () => {
    for(var k in units){
        let unit = units[k];
        unit.execute();
    }
}