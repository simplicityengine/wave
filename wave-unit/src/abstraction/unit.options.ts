export interface UnitOptions {
    type?: new () => any;
    method?: string;
    name?: string;
}