# README #
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* Quick summary
Wave is a Web framework sourced in [typescript](https://www.typescriptlang.org/) and deployed as **javascript** modules through [webpack](https://webpack.js.org/).


* Repo owner or admin
[Joseph Eniojukan](mailto://joseph@simplicityengine.com)

* Other community or team contact
[simplicityEngine](http://www.simplicityengine.com)