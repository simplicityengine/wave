export class VueComputed{
    public get: {()}
    public set: {(value)}

    public constructor(desc: PropertyDescriptor){
        this.get = desc.get;
        this.set = desc.set;
        delete desc.get;
        delete desc.set;
        desc.writable = true;
        desc.value = this;
    }
}