import Vue from "vue";
import {RouterOptions} from "../abstraction";
import {routes} from "../globalization";
import { VueConstructor } from "vue";
export function Router(options: RouterOptions){
    return function(ctor: VueConstructor<Vue>){
        options.component = ctor;
        routes.push(options);
    }
}