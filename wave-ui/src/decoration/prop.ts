import { VueProperty } from '../classification';
import { PropOptions } from "vue";
import { Argumenter } from "wave-core";

export function Prop(type: new () => any);
export function Prop(options: PropOptions);
export function Prop(...args){
    let argue = new Argumenter(args);
    let type = argue.function;
    let options = argue.object || <PropOptions>{type: type};
    return function(target: any, key: string){
       let desc = <PropertyDescriptor>{};
       new VueProperty(options, desc);
       Object.defineProperty(target, key, desc)
    }
}

export function p<T>(type: new () => T): T;
export function p(type: new () => any){
    let options = <PropOptions>{type: type};
    let desc = <PropertyDescriptor> {value: new type()}
    return new VueProperty(options, desc);
}