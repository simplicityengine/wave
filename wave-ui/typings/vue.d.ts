import VueRouter, { Route, NavigationGuard } from 'vue-router/types';
import { VuetifyObject } from './vuetify';

declare interface Vue {
    $router?: VueRouter;
    $route?: Route;
    $vuetify?: VuetifyObject;
}

declare interface VueConstructor{
    $router?: VueRouter;
    $route?: Route;
    $vuetify?: VuetifyObject;
}


declare interface ComponentOptions<V extends Vue> {
    path?: string | Array<string>;
    router?: VueRouter;
    beforeRouteEnter?: NavigationGuard;
    beforeRouteLeave?: NavigationGuard;
    beforeRouteUpdate?: NavigationGuard;
}