import Vue, { ComponentOptions } from "vue";

declare class VueClass<V extends Vue>{
    new(): VueClass<V>
    new(options?: ComponentOptions<V>): VueClass<V>
}