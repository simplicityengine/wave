import "vuetify/src/util/helpers";
import {PluginFunction} from "vue";

declare module "vuetify" {
  export default class Vuetify {
    static install: PluginFunction<never>;
  }

  export class VuetifyApplication {
    bar: number;
    bottom: number;
    left: number;
    right: number;
    top: number;
  }

  export class VuetifyBreakpoint {
    height: number;
    lg: boolean;
    lgAndDown: boolean;
    lgAndUp: boolean;
    lgOnly: boolean;
    md: boolean;
    mdAndDown: boolean;
    mdAndUp: boolean;
    mdOnly: boolean;
    name: string;
    sm: boolean;
    smAndDown: boolean;
    smAndUp: boolean;
    smOnly: boolean;
    width: number;
    xl: boolean;
    xlOnly: boolean;
    xs: boolean;
    xsOnly: boolean;
  }
  export class VuetifyTheme {
    primary: string;
    accent: string;
    secondary: string;
    info: string;
    warning: string;
    error: string;
    success: string;
  }
  
  export class VuetifyObject {
    application: VuetifyApplication;
    breakpoint: VuetifyBreakpoint;
    dark: boolean;
    theme: VuetifyTheme;
    touchSupport: boolean;
  }
}

import {VuetifyObject} from "vuetify"
declare interface Vue {
  $vuetify: VuetifyObject;
}