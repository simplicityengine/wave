import {Authentication} from "../abstraction";
import {Argumenter} from "../classification";
import { format, ok } from "../reflection";

export class BearerAuthentication implements Authentication {
    public bearer: string;
    public constructor();
    public constructor(bearer:string)
    public constructor(...args){
        let argue = new Argumenter(args);
        this.bearer = argue.string;
    }

    public get authorization(): string {
        return format("Bearer {}", this.bearer)
    }
}