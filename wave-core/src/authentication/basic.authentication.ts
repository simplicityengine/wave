import {Authentication} from "../abstraction";
import {Argumenter} from "../classification";
import { format, ok } from "../reflection";

export class BasicAuthentication implements Authentication{
    public username: string;
    public password: string;
    public domain: string;

    public constructor();
    public constructor(username: string, password: string);
    public constructor(username: string, password: string, domain: string)
    public constructor(...args){
        let argue = new Argumenter(args);
        this.username = argue.string;
        this.password = argue.string;
        this.domain = argue.string;
    }

    public get authorization(): string{
        let username = this.username;
        let password = this.password;
        let domain = this.domain;
        let encoded:string;

        if(ok(domain))
            encoded = btoa(format("{username}:{password}:{domain}", this));
        else
            encoded = btoa(format("{username}:{password}", this));
        
        return format("Basic {}", encoded);
    }
}