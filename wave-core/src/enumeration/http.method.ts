/**
 * @author Joseph Eniojukan
 * @description The {@link HttpMethod} represents the different types of <b>HTTP</b>
 * methods that can be sent to a server.
 */
export enum HttpMethod {
    CONNECT, 
    DELETE,
    GET,
    HEAD,
    OPTIONS,
    PATCH,
    POST,
    PUT,
    TRACE
}