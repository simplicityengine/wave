/**
 * @author Joseph Eniojukan
 * @description The {@link SchemaType} represents the different ways that the payload can
 * be <b>formated</b> when sent to or received from a <b>HTTP</b> service/api.
 */
export enum SchemaType {
    JSON, XML, HTML, MULTIPART, URLENCODED
}