export function primitive(arg): boolean{
    let type = typeof arg;
    return /string|number|boolean/i.test(type);
}