import { Definable } from '../definition';

export function ok(value): boolean {
    if(typeof value === 'undefined')
        return false;

    else if(value === null)
        return false;

    else if(typeof value === 'number')
        return true;

    else if(typeof value === 'boolean')
        return value;

    else if(typeof value === 'string')
        return value.trim().length > 0;

    else if(value instanceof Definable)
        return value.ok;

    else if(typeof value === 'object'){
        if(isNaN(value))
            return false;
    }

    return true;
}