export function dive(object, path: string) {
    var paths = path.split('.')
    let current = object;

    for (let i = 0; i < paths.length; i++) {
        let path = paths[i];
        //console.log(path);
        //console.log(current[path]);
        if (current[path] === undefined) {
            return undefined;
        } 
        else {
            current = current[path];
        }
        
    }
    return current;
}
