import { singular, format } from "../reflection";
import { Argumenter } from "../classification";

export function plural(object: Object): string;
export function plural(type: { new (): any }): string;
export function plural(...args){
    let argue = new Argumenter(args);
    let type: {new (): any} = argue.function;
    let object = argue.object;
    let name = (type ? singular(type) : singular(object))

    if(name){
        if(name.endsWith('quy'))
            return name.replace(/y$/i, 'ies');

        else if (name.match(/.*[aeiou]y$/i))
            return format('{}s', name);

        else if(name.endsWith('um'))
            return name.replace(/um$/i, 'a');

        else if(name.endsWith('y'))
            return name.replace(/y$/i, 'ies');

        else if(name.endsWith('ix'))
            return name.replace(/ix$/i, 'ices');

        else if(name.match(/.*[shx]$/i))
            return format('{}es', name);

        return format('{}s', name);
    }
}