import { Extension } from "../abstraction";
import {Definable} from "../definition"
export var definitions:{[key: string]: new () => Definable} = {};
export var extenstions:{[key: string]: new () => Extension} = {};