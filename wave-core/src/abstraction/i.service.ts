import { Authentication, ServiceResponse } from "../abstraction";
import { ValueMap } from "../classification";
import { HeaderMap, ParameterMap, QueryMap, ResourceMap } from "../communication";
import { Definable } from "../definition";

export interface IService {
    name: string;
    type: new() => Definable;
    url: string;
    authentication: Authentication

    uri(): string;
    uri(resources: Definable): string;
    uri(resources: ResourceMap): string;

    query(value: Definable): void;
    query(key: string): any;
    query(key: string, value: string): void;
    query(key: string, value: number): void;
    query(key: string, value: boolean): void;
    query(key: string, value: Definable): void;

    header(value: Definable): void;
    header(key: string): any;
    header(key: string, value: string): void;
    header(key: string, value: number): void;
    header(key: string, value: boolean): void;
    header(key: string, value: Definable): void;

    resource(value: Definable): void;
    resource(key: string): any;
    resource(key: string, value: string): void;
    resource(key: string, value: number): void;
    resource(key: string, value: boolean): void;
    resource(key: string, value: Definable): void;

    post(): Promise<ServiceResponse<Definable>>;
    post(payload: Definable): Promise<ServiceResponse<Definable>>;
    post(payload: FormData): Promise<ServiceResponse<Definable>>;
    post(payload: Definable, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    post(payload: FormData, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    post(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;
    
    patch(): Promise<ServiceResponse<Definable>>;
    patch(payload: Definable): Promise<ServiceResponse<Definable>>;
    patch(payload: FormData): Promise<ServiceResponse<Definable>>;
    patch(payload: Definable, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    patch(payload: FormData, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    patch(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;
    
    put(): Promise<ServiceResponse<Definable>>;
    put(payload: Definable): Promise<ServiceResponse<Definable>>;
    put(payload: FormData): Promise<ServiceResponse<Definable>>;
    put(payload: Definable, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    put(payload: FormData, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    put(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;

    delete(): Promise<ServiceResponse<Definable>>;
    delete(payload: Definable): Promise<ServiceResponse<Definable>>;
    delete(payload: FormData): Promise<ServiceResponse<Definable>>;
    delete(payload: Definable, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    delete(payload: FormData, ...maps: Array<ValueMap> ): Promise<ServiceResponse<Definable>>;
    delete(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;
   
    get(): Promise<ServiceResponse<Definable>>;
    get(filter: Definable): Promise<ServiceResponse<Definable>>;
    get(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;
    
    head(): Promise<ServiceResponse<Definable>>;
    head(filter: Definable): Promise<ServiceResponse<Definable>>;
    head(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;

    options(): Promise<ServiceResponse<Definable>>;
    options(filter: Definable): Promise<ServiceResponse<Definable>>;
    options(filter: Object): Promise<ServiceResponse<Definable>>;
    options(...maps: Array<ValueMap>): Promise<ServiceResponse<Definable>>;
}