import { Definable } from "../definition"
import { HeaderMap } from "../communication";
import { HttpMethod } from "../enumeration";
export interface ServiceResponse<D extends Definable> {
    code?: number;
    method?: HttpMethod
    headers?: HeaderMap
    data?: D | Array<D>
}