import { uuid } from "../utilization";
import {Argumenter} from "../classification"
import { ok } from "../reflection";

export class CallFlow {
    private sequence = new Array<CallBack>();

    public set callback(value: { (thread?: CallThread) }) {
        this.push(value);
    }
    public push(name: string, callback: { (thread?: CallThread) });
    public push(callback: { (thread?: CallThread) });
    public push(callback: { (thread?: CallThread) }, name: string);
    public push(...args) {
        let argument = new Argumenter(args);
        let name: string = argument.string;
        let callback: { (thread?: CallThread) } = argument.function;
        this.sequence.push(new CallBack(name, callback))
    }

    public start();
    public start(name: string);
    public start(name: string, delay: number);
    public start(delay: number);
    public start(delay: number, name: string);
    public start(...args) {
        let thread = new CallThread(this);
        let param = new Argumenter(args);
        let name: string = param.string;
        let delay: number = param.number;

        if (ok(name))
            thread.goto(name, delay);
        else
            thread.next(delay);
    }
}

export class CallThread {
    private before = new Array<string>();
    private started: boolean = false;
    constructor(private callflow: CallFlow) {
        ['_name', '_index'].forEach(key => {
            Object.defineProperty(this, key, { writable: true, enumerable: false })
        })
        this.setIndex(-1);
    }

    public next();
    public next(delay: number);
    public next(delay?: number) {
        this.setIndex(this.index + 1);
        let callback = this.getIndex(this.index);

        if (callback)
            callback.execute(this, delay);
    }

    public previous();
    public previous(delay: number);
    public previous(delay?: number) {
        let before = this.before.pop();
        let callback = this.getName(before);
        if (callback)
            callback.execute(this, delay);
    }

    public goto(name: string);
    public goto(name: string, delay: number);
    public goto(name: string, delay?: number) {
        let callback = this.getName(name);
        if (callback) {
            this.setIndex(this.indexOf(name));
            callback.execute(this);
        }
    }

    public history(name: string) {
        this.before.push(name);
    }

    private getName(name: string) {
        let result: CallBack;
        let sequence = this.sequence;

        sequence.forEach(callback => {
            if (callback.name === name) {
                result = callback;
                return;
            }
        })

        return result;
    }

    private getIndex(index: number): CallBack {
        let sequence = this.sequence;
        if (index < sequence.length)
            return sequence[index];
    }

    private indexOf(name: string): number {
        let result: number;
        let sequence = this.sequence;

        for (let i = 0; i < sequence.length; i++) {
            let callback = sequence[i];
            if (callback.name === name) {
                return i;
            }
        }

        return result;
    }

    private hasName(name: string): boolean {
        let callback = this.getName(name);
        if (callback)
            return true;

        return false;
    }

    private get sequence(): Array<CallBack> {
        return this.callflow['sequence'];
    }

    public get name(): string {
        return this['_name'];
    }

    public get index(): number {
        return this['_index'];
    }

    private setName(value: string) {
        this['_name'] = value;
    }

    private setIndex(value: number) {
        this['_index'] = value;
    }

}

class CallBack {
    constructor(public name: string, private callback: { (thread?: CallThread) }) {
        if (!ok(this.name))
            this.name = uuid();
    }

    public execute(thread: CallThread, delay: number = -1) {
        if (delay > 0) {
            setTimeout(() => this.callback(thread), delay);
        }
        else
            this.callback(thread);
    }
}