/**
 * @author Joseph Eniojukan
 * 
 * @description The <b>Argumenter</b> class allows developers to parse through an array of 
 * arguments via the argument type.<br/><br/>
 * The {@link Argumenter} proves very useful for <b>functions</b> and <b>constructors</b> that have multiple parameters/arguments passed to it.
 * Normally, <b>javascript/typescript</b> developers will use <b>options</b> (i.e a mapped object) to pass
 * information to a function or a constructor (ex: `function({name: 'John', age: 26})`).<br/><br/>
 * However, typescript allows clear method overloading definitions that allow developers more freedom
 * on how they define function signatures.  With this capability, exposed/exported functions can be
 * written in such a way to allow flexibility for those who consume them.  The functions also resemble
 * standard OOP methods in languages like <b>Java</b>; making it more familiar for those whom come from 
 * that type of development background.<br/><br/>
 * 
 * @example The following shows how the augmenter can isolate different argument types <b>no matter</b> 
 * the <b>order</b> that they are passed into the function.<br/><br/>
 * <pre>
 * hello();
 * hello(name: string);
 * hello(age: number);
 * hello(name: string, age: number);
 * hello(age: number, name: string);<br/>
 * hello(...args){
 *      var argue = new Argumenter(args);
 *      var name = argue.string;
 *      var age = argue.number;<br/>
 *      if(name &amp;&amp; age){
 *          return "Hello " + name + "\nyou are " + age + " years old.";
 *      }<br/>
 *      else if(name){
 *          return "Hello " + name;
 *      }<br/>
 *      else if(age){
 *          return "Hello\nyou are " + age + " years old.";
 *      }<br/>
 *      return "Hello\nI do not know your age.";
 * }
 * </pre><br/>
 */
export class Argumenter {
    private args = [];
    constructor(args: Array<any>) {
        if (Array.isArray(args)) {
            for (let i = 0; i < args.length; i++) {
                let arg = args[i];
                this.args.push(arg);
            }
        }
    }
    /** */
    public get number(): number {
        return this.getNative('number');
    }

    public get string(): string {
        return this.getNative('string');
    }

    public get boolean(): boolean {
        return this.getNative('boolean');
    }

    public get nil(): null{
        return this.getNative('null');
    }

    public get null(): null{
        return this.getNative('null');
    }

    public get object(): Object {
        return this.getNative('object');
    }

    public get function(): any {
        return this.getNative('function');;
    }

    public get array(): Array<any> {
        return this.getNative('array');
    }

    public instance<T>(type: { new (): T }): T {
        let args = this.args;
        let result;
        let remove = -1;
        for (let i = 0; i < args.length; i++) {
            let arg = args[i];
            if (arg instanceof type) {
                result = arg;
                remove = i;
                break;
            }
        }

        if (remove >= 0)
            args.splice(remove, 1);

        return result;
    }
    private screenArray(type: string, arg: any, arrayed: boolean) {
        if (type === 'object') {
            if (Array.isArray(arg))
                return arrayed;
        }

        return true;
    }

    private getNative(type: string) {
        let args = this.args;
        let result;
        let remove = -1;
        let arrayed = false;

        if (type === 'array') {
            type = 'object';
            arrayed = true;
        }
        for (let i = 0; i < args.length; i++) {
            let arg = args[i];
            if (typeof arg === type && this.screenArray(type, arg, arrayed)) {
                result = arg;
                remove = i;
                break;
            }
        }

        if (remove >= 0)
            args.splice(remove, 1);

        return result;
    }
}
