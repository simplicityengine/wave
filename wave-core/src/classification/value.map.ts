import {Argumenter} from "../classification";
import {ok, primitive} from '../reflection';
import { Definable } from "../definition";
export class ValueMap{
    public pairs: {[key: string]: any} = {};

    public constructor();
    public constructor(value: object);
    public constructor(key: string, value: string);
    public constructor(key: string, value: number);
    public constructor(key: string, value: boolean);
    public constructor(key: string, value: object);
    public constructor(a?: any, b?: any){
        if(typeof a === 'object'){
            this.set(a);
        }
        else if(typeof a === 'string'){
            if(primitive(b) || b === null || typeof b === 'object')
                this.set(a, b);
        }
    }

    public copy(map: ValueMap){
        if(map instanceof ValueMap){
            let pairs = map.pairs;
            for(let k in pairs){
                let v = pairs[k];
                this.set(k, v);
            }
        }
    }

    // TODO: overload with object
    public get(key: string){
        return this.pairs[key];
    }

    // TODO: overload with object
    public delete(key: string){
        delete this.pairs[key];
    }

    public set(value: object)
    public set(key: string, value: string);
    public set(key: string, value: number);
    public set(key: string, value: boolean);
    public set(key: string, value: object);
    public set(...args): this{
        let argue = new Argumenter(args);
        let pairs = this.pairs;
        if(args.length === 1){
            let object = argue.object;
            if(object){
                for(let k in object){
                    let v = object[k];
                    if(primitive(v) || v === null)
                        this.set(k, v);
                    else if(v instanceof Definable)
                        this.set(k, v.id);
                }
            }
        }
        else if (args.length === 2){
            let key = argue.string;
            let string = argue.string;
            let number = argue.number;
            let boolean = argue.boolean;
            let nil = argue.nil;
            let object = argue.object;

            if(ok(key)){
                if(ok(string))
                    pairs[key] = string;
                
                else if(ok(number))
                    pairs[key] = number;
                
                else if(ok(boolean))
                    pairs[key] = boolean;

                else if(nil === null)
                    pairs[key] = null;

                else if(object != undefined){
                    let value = object[key];
                    if(primitive(value) || value === null)
                        pairs[key] = value;
                }
            }
        }
        return this;
    }

    public add(value: object)
    public add(key: string, value: string);
    public add(key: string, value: number);
    public add(key: string, value: boolean);
    public add(key: string, value: object);
    public add(...args): this{
        let pairs = this.pairs;
        let argue = new Argumenter(args);

        if(args.length === 1){
            let object = argue.object;
            if(object){
                for(let k in object){
                    let v = object[k];
                    if(primitive(v) || v === null)
                        this.add(k, v);
                    else if(v instanceof Definable)
                        this.add(k, v.id);
                }
            }
        }
        else if(args.length === 2){
            let key = argue.string;
            let string = argue.string;
            let number = argue.number;
            let boolean = argue.boolean;
            let nil = argue.nil;
            let array = argue.array;
            let object = argue.object;

            if(ok(key)){
                if(key in pairs){
                    if(!array){
                        array = [];
                        pairs[key] = array;
                    }
                    if(ok(string))
                        array.push(string);
                
                    else if(ok(number))
                        array.push(number);
                    
                    else if(ok(boolean))
                        array.push(boolean);

                    else if(nil === null)
                        array.push(null);

                    else if(object){
                        let value = object[key];
                        if(primitive(value) || value === null)
                            array.push(value);
                    }
                }
                else {
                    if(ok(string))
                        pairs[key] = string;
                
                    else if(ok(number))
                        pairs[key] = number;
                    
                    else if(ok(boolean))
                        pairs[key] = boolean;

                    else if(nil === null)
                        pairs[key] = null;

                    else if(object != undefined){
                        let value = object[key];
                        if(primitive(value) || value === null)
                            pairs[key] = value;
                    }
                }
                
            }
        }
        return this;
    }
}