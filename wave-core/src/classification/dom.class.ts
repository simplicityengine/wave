export class DOMClass {
    public static verbose: boolean = false;
    public static MIME_TYPE: string = "application/xml";
    private parser:DOMParser = new DOMParser();

    public constructor(content: string);
    public constructor(node: Node);
    public constructor(arg?: string | Node){
        ['node', 'verbose'].forEach(name => {
            Object.defineProperty(this, '_' + name, {enumerable: true, configurable: true, writable: true});
        })
        
        if(typeof arg === 'string'){
            this.load(arg);
        }
        else if(arg instanceof Node){
            this.node = arg;
        }
    }

    public get verbose(): boolean{
        return this['_verbose'] || DOMClass.verbose;
    }

    public set verbose(value: boolean){
        this['_verbose'] = value;
    }

    public get node(): Node{
        return this['_node'];
    }

    public set node(value: Node){
        this['_node'] = value;
    }

    public get document(): Document{
        let node = this.node;
        if(node.nodeType === Node.DOCUMENT_NODE){
            return <Document> node;
        }

        return undefined;
    }

    public set document(value: Document){
        this.node = value;
    }

    public get element(): Element {
        let node = this.node;
        if(node){
            if(node.nodeType == Node.ELEMENT_NODE){
                return <Element> node;
            }
            else if(node.nodeType === Node.DOCUMENT_NODE){
                return this.document.firstElementChild;
            }
        }
        return undefined;
    }

    public set element(value: Element){
        this.node = value;
    }

    public getFirstElementByTagName(tag: string): Element{
        let doc = this.document;
        if(doc){
            let nodes = doc.getElementsByTagName(tag);
            if(nodes.length > 0){
                return nodes[0];
            }
        }
        return undefined;
    }

    public load(content: string): void {
        let parser = this.parser;
        let doc = this.node = parser.parseFromString(content, DOMClass.MIME_TYPE);

        if(doc){
            let element: Element = doc.firstElementChild;
            if(element.tagName === 'parsererror'){
                let msg = (this.verbose ? element.textContent : element.firstChild.textContent);
                let error = new Error(msg);
                throw error;
            }
        }
    }

}