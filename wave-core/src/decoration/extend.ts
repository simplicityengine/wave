import {Extension} from "../abstraction";
import {extenstions} from "../globalization";
export function Extend() {
    return function(type: new() => Extension){
        let key:string = type['name'].toLowerCase();
        extenstions[key] = type;
    }
}