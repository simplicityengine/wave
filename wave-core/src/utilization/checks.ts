export function isBrowser(): boolean{
    try{
        return typeof window !== 'undefined';
    }
    catch(err){
    }
    return false;
}

export function isJSON(content: string): boolean{
    try{
        JSON.parse(content);
        return true;
    }
    catch(e){}
    return false;
}

export function isXML(content: string): boolean{
    if(isBrowser()){
        let parser = new DOMParser();
        let doc = parser.parseFromString(content, "text/xml");

        let element = doc.firstElementChild;

        if(element){
            return element.tagName !== 'parsererror';
        }

        return false;
    }
    else
        throw new Error("method not supported in a node environment");
}