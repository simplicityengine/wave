import { Authentication , IService, ServiceResponse} from "../abstraction";
import { Definable } from "../definition";
import { singular, plural, ok, copy, clone, format, primitive } from "../reflection";
import { HttpMethod } from "../enumeration";
import { Argumenter, ValueMap } from "../classification";
import { json } from "../transformation";
import { HeaderMap, ResourceMap, ParameterMap, QueryMap } from "../communication";


export abstract class Service<D extends Definable, M extends ValueMap> implements IService{
    protected array: Array<D>;
    protected queries: QueryMap = new QueryMap();
    protected headers: HeaderMap = new HeaderMap();
    protected parameters: ParameterMap = new ParameterMap();
    protected resources: ResourceMap = new ResourceMap();

    public static url: string;
    public static authentication: Authentication
    
    public constructor(type: { new(): D })
    public constructor(type: { new(): D }, uri: string)
    public constructor(type: { new(): D }, uri: string, name: string)
    public constructor(type: { new(): D }, uri: string, name: string, url: string)
    public constructor()
    public constructor(...args){
        ['url', 'uri', 'name', 'type', 'authentication'].forEach(key => Object.defineProperty(this, '_' + key, {writable: true, enumerable: false}))
        let argue = new Argumenter(args);
        let type: { new(): D } = argue.function;
        let uri: string = argue.string || plural(type);
        let name: string = argue.string || plural(type);
        let url: string = argue.string || Service.url;
        
        this.type = type;
        this['_uri'] = uri;
        this.name = name;
        this.url = url;
        this.array = new Array();
        this.array.type = type;
    }

    public get type(): {new (): D}{
        return this['_type'];
    }

    public set type(value) {
        let array = this.array;
        this['_type'] = value;
        if(array)
            array.type = value;
    }
    public get name(): string{
        return this['_name'] || plural(this.type);
    }

    public set name(value: string){
        this['_name'] = value;
    }

    public get url(): string{
        return this['_url'] || Service.url;
    }

    public set url(value: string){
        this['_url'] = value;
    }

    public get path(): string{
        let url = this.url;
        let uri = this.uri;
        
        let path = format("{url}/{uri}", {url: url || '', uri: uri || ''})

        return path;
    }

    public get authentication(): Authentication {
        return this['_authentication'] || Service.authentication;
    }

    public set authentication(value: Authentication) {
        this['_authentication'] = value;
    }

    public uri(): string;
    public uri(resources: D): string;
    public uri(resources: ResourceMap): string;
    public uri(...args): string{
        let argue = new Argumenter(args);
        let resources = argue.instance(Definable) || argue.instance(ResourceMap) || this.resources || {};
        let uri: string = this['_uri'];

        for(let k in resources){
            let v = resources[k];
            uri = uri.replace(":" + k, String(v));
            uri = uri.replace(new RegExp('^\{\s*' + k + '\s*}\$', 'i'), String(v))
        }
        return uri;
    }

    public query(value: D): void;
    public query(key: string): any;
    public query(key: string, value: string): void;
    public query(key: string, value: number): void;
    public query(key: string, value: boolean): void;
    public query(key: string, value: D): void;
    public query(...args){
        args.unshift(this.queries);
        return this.map(args);
    }

    public header(value: D): void;
    public header(key: string): any;
    public header(key: string, value: string): void;
    public header(key: string, value: number): void;
    public header(key: string, value: boolean): void;
    public header(key: string, value: D): void;
    public header(...args){
        args.unshift(this.headers);
        return this.map(args);
    }

    public resource(value: D): void;
    public resource(key: string): any;
    public resource(key: string, value: string): void;
    public resource(key: string, value: number): void;
    public resource(key: string, value: boolean): void;
    public resource(key: string, value: D): void;
    public resource(...args){
        args.unshift(this.resources);
        return this.map(args);
    }


    protected map(args: Array<any>){
        let length = args.length;
        let argue = new Argumenter(args);
        let definable = <D> argue.instance(Definable)
        let map = argue.instance(ValueMap);
        let key = argue.string;
        let string = argue.string;
        let number = argue.number;
        let boolean = argue.boolean;
        

        let value = undefined;

        if(ok(string))
            value = string;

        else if(ok(number))
            value = number;

        else if(ok(boolean))
            value = boolean;

        else if(definable){
            value = definable.dive(key);
        }

        switch(length){
            case 2:
                if(definable){
                    for(let k in definable){
                        let v:any = definable[k];
                        if(primitive(v))
                            map.set(k, v)
                        else if(v instanceof Definable){
                            if(ok(v))
                                map.set(k, v.id);
                        }
                            
                    }
                }
                else
                    return map.get(key);
                
                break;
            case 3:
                ( value != undefined ? map.set(key, value) : map.delete(key) );
                break;
        }
    }

    public post(): Promise<ServiceResponse<D>>;
    public post(payload: D): Promise<ServiceResponse<D>>;
    public post(payload: FormData): Promise<ServiceResponse<D>>;
    public post(payload: D, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public post(payload: FormData, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public post(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public post(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.POST);
        return this.streamed(args);
    }

    public patch(): Promise<ServiceResponse<D>>;
    public patch(payload: FormData): Promise<ServiceResponse<D>>;
    public patch(payload: D, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public patch(payload: FormData, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public patch(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public patch(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.PATCH);
        return this.streamed(args);
    }

    public put(): Promise<ServiceResponse<D>>;
    public put(payload: FormData): Promise<ServiceResponse<D>>;
    public put(payload: D, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public put(payload: FormData, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public put(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public put(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.PUT);
        return this.streamed(args);
    }

    public delete(): Promise<ServiceResponse<D>>;
    public delete(payload: FormData): Promise<ServiceResponse<D>>;
    public delete(payload: D, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public delete(payload: FormData, ...maps: Array<M> ): Promise<ServiceResponse<D>>;
    public delete(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public delete(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.DELETE);
        return this.streamed(args);
    }

    protected abstract streamed(args: Array<any>): Promise<ServiceResponse<D>>;

    public get(): Promise<ServiceResponse<D>>;
    public get(filter: D): Promise<ServiceResponse<D>>;
    public get(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public get(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.GET);
        return this.streamless(args);
    }

    public head(): Promise<ServiceResponse<D>>;
    public head(filter: D): Promise<ServiceResponse<D>>;
    public head(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public head(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.HEAD);
        return this.streamless(args);
    }

    public options(): Promise<ServiceResponse<D>>;
    public options(filter: D): Promise<ServiceResponse<D>>;
    public options(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public options(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.OPTIONS);
        return this.streamless(args);
    }

    public trace(): Promise<ServiceResponse<D>>;
    public trace(filter: D): Promise<ServiceResponse<D>>;
    public trace(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public trace(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.TRACE);
        return this.streamless(args);
    }

    public connect(): Promise<ServiceResponse<D>>;
    public connect(filter: D): Promise<ServiceResponse<D>>;
    public connect(...maps: Array<M>): Promise<ServiceResponse<D>>;
    public connect(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.CONNECT);
        return this.streamless(args);
    }
    
    protected abstract streamless(args: Array<any>): Promise<ServiceResponse<D>>;
}