import { ValueMap } from "../classification";

export class ParameterMap extends ValueMap {
    public constructor();
    public constructor(value: object);
    public constructor(key: string, value: string);
    public constructor(key: string, value: number);
    public constructor(key: string, value: boolean);
    public constructor(key: string, value: object);
    public constructor(...args){
        switch(args.length){
            case 1: super(args[0]); break;
            case 2: super(args[1]); break;
            default: super(); break;
        }
    }
}