import { clear, inject, copy } from "../reflection";
import { Extension } from "../abstraction";
import { Extend } from "../decoration";
@Extend()
export class ArrayExtension implements Extension {
    public static extended: boolean = false;
    public get extended(){
        return ArrayExtension.extended;
    }
    public extend() {
        if(ArrayExtension.extended)
            return
        else
            ArrayExtension.extended = true;

        Array.prototype.empty = function(): boolean {
            return this.length <= 0;
        }
        Array.prototype.clear = function () {
            clear(this);
        }

        Array.prototype.inject = function <T>(type: { new (): T }) {
            this.type = type;
            inject(this, type);
        }

        Array.prototype.add = function <T>(item: T, key: string = "id") {
            let type: new () => T = this.type
            let object: T
            if (type) {
                if (item instanceof type)
                    object = item;
                else {
                    object = new type();
                    copy(item, object);
                }
            }
            else
                object = item;

            if (!this.has(object, key))
                this.push(object);
        }

        Array.prototype.get = function <T>(item: string | number | T, key: string = "id") {
            let id: any;
            if (typeof item === 'string' || typeof item === 'number')
                id = item;

            else if(typeof item === 'object')
                id = item[key];

            for (let i = 0; i < this.length; i++) {
                let child: T = this[i];
                if (child[key] === id)
                    return child;
            }
        }

        Array.prototype.has = function <T>(item: string | number | T, key: string = "id") {
            if (this.get(item, key))
                return true;
            return false;
        }

        Array.prototype.drop = function <T>(item: string | number | T, key: string = "id") {
            let id: any;
            if (typeof item === 'string' || typeof item === 'number')
                id = item;
            else
                id = item[key];

            for (let i = this.length - 1; i >= 0; i--) {
                let child: T = this[i];
                if (child[key] === id) {
                    this.splice(i, 1);
                    break;
                }

            }
        }

        Array.prototype.index = function <T>(item: string | number | T, key: string = "id"): number {
            let id: any;
            if (typeof item === 'string' || typeof item === 'number')
                id = item;
            else
                id = item[key];
            
            for (let i = this.length - 1; i >= 0; i--) {
                let child: T = this[i];
                if (child[key] === id) {
                    return i;
                }

            }
        }

        Array.prototype.replace = function <T>(search: string | number | T, item: string | number | T, key: string = "id") {
            let index = this.index(search, key);
            if(typeof index === 'number' && index >= 0){
                this.splice(index, 1, item);
            }
            else
                this.push(item);
        }



        Array.prototype.max = function <T>(key: string = "id") {
            let current: T;
            for (let i = 0; i < this.length; i++) {
                let item = this[i];
                if (!current)
                    current = item;

                if (item[key] > current[key]) {
                    current = item;
                }
            }

            return current;
        }

        Object.defineProperty(Array.prototype, 'first', {
            get: function(){
                if(this.length > 0)
                    return this[0];
            },
            set: function(value){

            }
        });

        Object.defineProperty(Array.prototype, 'last', {
            get: function(){
                if(this.length > 0)
                    return this[this.length - 1];
            },
            set: function(value){

            }
        });
    }
}