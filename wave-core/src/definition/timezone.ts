import {Enumerable, Country, Continent} from "../definition"
export class Timezone extends Enumerable{
    public code: string;
    public alternate: string;
    public offset: number;
}