import {Enumerable} from "../definition"
export class Geographical extends Enumerable {
    public code: string;
    public geoId: number;
    public friendlyName: string;
}