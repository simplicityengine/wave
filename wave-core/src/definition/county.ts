import {Geographical, Continent, Country} from "../definition"
export class County extends Geographical {
    public division: Division;
    public country: Country;
    public continent: Continent;
}
export class Division {
    public code: string;
    public name: string;
    public friendlyName: string;
}