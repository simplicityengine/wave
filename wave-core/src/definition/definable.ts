import { Argumenter } from "../classification";
import { clone, format, dive, singular, plural } from "../reflection";
import {definitions} from "../globalization";

export class Definable {
    public id: number
    constructor()
    constructor(id:number)
    constructor(...args){
        let argue:Argumenter = new Argumenter(args);
        let type:{new()} = <any> this['constructor'];
        let one = singular(type);
        let many = plural(type);

        [one, many].forEach( key => {
            if(!definitions[key])
                definitions[key] = type;
        })

        this.id = argue.number;
    }
    
    public get ok(): boolean{
        return this.id > 0;
    }
    public set ok(value){};

    public isId(data: Definable): boolean{
        if(!data)
            return false;
        
        if(!this.ok || !data.ok)
            return false;

        return this.id === data.id;
    }

    public dive(key: string){
        return dive(this, key);
    }
}

export function signature <D extends Definable> (definable: D): D
export function signature <D extends Definable> (type: new () => D): D 
export function signature <D extends Definable> (...args): D{
    let argue = new Argumenter(args);
    let definable: D = <D> argue.instance(Definable);
    let type: {new (): D} = argue.function;

    if(definable)
        type = <any> definable['constructor'];
    else if(type)
        definable = new type();

    let result = clone(definable);
    
    for(let key in result){
        if(key !== 'id')
            delete result[key];
    }
    return result;
}

export function define(key: string): new() => Definable{
    return definitions[key];
}