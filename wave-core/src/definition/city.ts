import {Geographical, Continent, Country, County} from "../definition"
export class City extends Geographical {
    public county: County;
    public country: Country;
    public continent: Continent;
    
}