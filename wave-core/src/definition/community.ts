import {Geographical, Continent, Country, County, City} from "../definition"
export class Community extends Geographical {
    public city: City;
    public county: County;
    public country: Country;
    public continent: Continent;
}