import {Definable} from "../definition"
import { Argumenter } from "../classification";
export class Enumerable extends Definable {
    public name: string;

    public constructor();
    public constructor(id: number);
    public constructor(id: number, name: string);
    public constructor(...args){
        super()
        let argue = new Argumenter(args);
        this.id = argue.number;
        this.name = argue.string;
    }
}