/** @type {Object.<string, Function> & {length: number, passed: number, failed: number, total: number} } */
let cases = { };
Object.defineProperty(cases, 'length', {
    enumerable: false,
    get: () => {
        let count = 0;
        for(var k in cases){
            count++;
        }
        return count;
    }
});

Object.defineProperty(cases, 'total', {
    enumerable: false,
    get: /**@this {Error}*/ () => {
        return cases.passed + cases.failed;
    }
});


['passed', 'failed'].forEach((key) => {
    Object.defineProperty(cases, key, {
        enumerable: false,
        writable: true,
        configurable: false,
        value: 0
    })
})
    

exports.cases = cases;
