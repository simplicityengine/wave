const {isBrowser} = require("../lib/utilization");

/**
 * @param {string} key 
 * @param {string} message 
 * @param {boolean} success 
 */
function log(key, message, success){
    let _console = (success ? '[PASSED]' : '[FAILED]') + ' [' + key + '] ' + message;
    (success ? console.info(_console) : console.warn(_console))
    if(isBrowser()){
        /**@type {HTMLTableElement} */
        let table = document.getElementById('results');
        let row = table.insertRow();
        let name = row.insertCell();
        let status = row.insertCell();
        let msg = row.insertCell();

        row.className = (success ? 'passed' : 'failed');
        name.innerHTML = '<b>' + key + '</b>';
        status.innerHTML = (success ? '[PASSED]' : '[FAILED]');
        msg.innerHTML = message;
    }
}

console.info("executing ", cases.length, " test cases")
for(let key in cases){
    let testcase = cases[key];
    let success = false;
    let message = "";

    try{
        testcase();
        message =  'executed successfully';
        success = true;
    }
    
    catch(error){
        message = error.message;
        success = false;
    }

    log(key, message, success);
}