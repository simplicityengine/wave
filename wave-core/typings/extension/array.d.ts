declare interface Array<T>{
    add: (item: T, key?: string) => void;
    empty: () => boolean;
    get: (item: number | string | T, key?: string) => T;
    has: (item: number | string | T, key?: string) => boolean;
    drop: (item: number | string | T, key?: string) => void;
    index: (item: number | string | T, key?: string) => number;
    replace: (search: number | string | T, item: number | string | T, key?: string) => void;
    max: (key?: string) => T;
    clear(): void;
    inject(type: new () => T);
    type: new () => T;
    first: T;
    last: T;
}