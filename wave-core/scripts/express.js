const {resolve} = require("path");
const {existsSync} = require("fs");
var express = require("express");
let docPath = resolve('lib/documentation');
let testPath = resolve('tests/')

if(existsSync(docPath)){
    const app = express();
    app.use('/test', express.static(testPath));
    app.use('/docs', express.static(docPath));
    app.listen(8080, () => console.log('ExressJS listening on port 8080!'));
}
