const fs = require("fs");
const {DOMParser, XMLSerializer} = require("xmldom-alpha");
const {format, ok} = require("../lib");
const {resolve, relative, sep} = require("path");
const {execSync} = require("child_process");
const package = require(resolve("package.json"));
var outputPath = resolve("lib/documentation");
var inputPath = resolve("src/documentation.ts");
var readmePath = resolve("README.md");

let dom = new DOMParser({
    errorHandler: {
        warning: (w) => {},
        error: (err) => {},
        fatalError: (err) => {console.info(err)}
    }
});

let xs = new XMLSerializer();
let options = {
    out: outputPath,
    name: package.name,
    mode: 'file',
    excludePrivate: true,
    readme: (fs.existsSync(readmePath) ? readmePath : 'none')
}

function command(options) {
    let sb = "npm run typedoc -- ";

    for(var key in options){
        let value = options[key];
        if(ok(value)){
            if(typeof value === 'boolean'){
                if(value){
                    sb += format(" --{key}", {key});
                }
            }
            else{
                sb += format(" --{key} {value}", {key, value});
            }
            
        }
    }
    sb += format(" {}", inputPath);
    return sb;
}

/**@param {string} path */
function purge(path){
    let regex = /_index_\.html$/i;
    let stat = fs.statSync(path);
    if(stat.isFile()){
        if(regex.test(path)){
            fs.unlinkSync(path)
        }
        else{
            let html = dom.parseFromString(fs.readFileSync(path, 'utf8'), 'text/html');
            let elements = html.getElementsByTagName("a");
            for(let i = 0; i < elements.length; i++){
                let a = elements[i];
                let li = a.parentNode;

                if(li){
                    if(li.tagName === 'li' && regex.test(a.getAttribute('href'))){
                        let parent = li.parentNode;
                        if(parent){
                            parent.removeChild(li);
                        }
                    }
                }
            }
            
            fs.writeFileSync(path, xs.serializeToString(html, true));
        }
    }
    else if(stat.isDirectory()){
        let files = fs.readdirSync(path);

        for(let i = 0; i < files.length; i++){
            let file = files[i];
            purge(format("{path}{sep}{file}", {path, sep, file}));
        }
    }  
}

if(fs.existsSync(outputPath)){
    console.info(format("removing path: {}", outputPath));
    execSync(format("rm -rf {}", outputPath))
}
console.info(execSync(command(options),{encoding: "utf8"}));
//purge(outputPath);
