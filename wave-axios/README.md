# README #

An axios based http client for the wave framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

**01)** [nodejs](https://nodejs.org/en/)<br/>
**02)** [vscode](https://code.visualstudio.com/)<br/>
**03)** [typescript](https://www.npmjs.com/package/typescript)<br/>
**04)** [axios](https://www.npmjs.com/package/axios)<br/>
**05)** [wave-core](https://npmregistry.simplicitylabs.ca/#/detail/wave-core)<br/>


### Installing

Do the following steps to install **wave-axios**:

**01)** Setup credentials to the **simplicityLabs** NPM registry
```
npm adduser --registry https://npmregistry.simplicitylabs.ca
```
**NOTE:** contact the [registry administrator](mailto:joseph@simplicitylabs.ca?Subject=NPM%20Registry%20Access) to get your [bitbucket](https://bitbucket.org) user account mapped to the [simplicityLabs registry](https://npmregistry.simplicitylabs.ca).  Use your [bitbucket](https://bitbucket.org) password when prompted.

**02)** Install the **wave-axios** npm package
```
npm install wave-axios --save --registry https://npmregistry.simplicitylabs.ca
```

## Publish

**01)** To tranpile the source into javascript, execute the following:<br/>
`npm run build`

**02)** To publish a patch on the [simplicityLabs registry](https://npmregistry.simplicitylabs.ca), execute the following:<br/>
`npm run patch`

## Built With

**01)** [wave-core](https://bitbucket.org/simplicityengine/wave) - Utilities Core<br/>
**02)** [axios](https://github.com/axios/axios) - Http Client<br/>

## Authors

**01)** **Joseph Eniojukan** - *Initial work* - [joejukan](https://bitbucket.org/joejukan/)<br/>


## License

This project is licensed under the ISC License - see the **LICENSE.md** file for details

Copyright 2017 simplicityEngine

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.