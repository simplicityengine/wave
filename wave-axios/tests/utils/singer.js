var core = require("wave-core");
var Definable = core.Definable;
var Argumenter = core.Argumenter;

function Singer(...args){
    var argue = new Argumenter(args);
    this.id = argue.number;
    this.firstname = argue.string;
    this.lastname = argue.string;
    this.stagename = argue.string;
    this.age = argue.number;
}

Singer.prototype = new Definable();
/**@type {string} */
Singer.prototype.firstname = undefined;

/**@type {string} */
Singer.prototype.lastname = undefined;

/**@type {string} */
Singer.prototype.stagename = undefined;

/**@type {number} */
Singer.prototype.age = undefined;

exports = Singer;