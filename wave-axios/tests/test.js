var lib = require("../lib");

var utils = require("./utils")
var core = require("wave-core");

let AxiosService = lib.AxiosService;
let AxiosFault = lib.AxiosFault;
let format = core.format;

let Singer = utils.Singer;

AxiosService.url = "http://localhost:8080"
let service = new AxiosService(Singer,'/singers', 'singers');

service.get()
.then(singers => {
    
    //console.info(singers.data)
})

.catch( /**@param {AxiosFault} fault*/ (fault) => {
    console.info(format("[{code}] {message}", fault))
})
