import { Definable, Argumenter, plural, ok, primitive,  json, clone, copy, format} from "wave-core";
import { HttpMethod, Service, ServiceResponse, Authentication, ValueMap, HeaderMap, QueryMap, ResourceMap } from "wave-core";

import {AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosPromise, AxiosError} from "axios";
import {AxiosFault} from "../exception";
import axios from "axios";

export class AxiosService<D extends Definable, M extends ValueMap > extends Service<D, M>{
    /** the attribute path to find the error message */
    public static errorMessagePath: string = 'message';

    /** the attribute path to find the error message */
    public errorMessagePath: string = AxiosService.errorMessagePath;

    public static get url(): string{
        return Service.url;
    }

    public static set url(value: string){
        Service.url = value;
    }

    public config(): AxiosRequestConfig;
    public config(...maps: Array<ValueMap>): AxiosRequestConfig;
    public config(...args): AxiosRequestConfig{
        let argue = new Argumenter(args);
        let config:AxiosRequestConfig = {};
        let authentication = this.authentication;

        let headers = argue.instance(HeaderMap);
        let queries = argue.instance(QueryMap);
        let resources = argue.instance(ResourceMap);

        if(headers)
            headers.copy(this.headers);
        else
            headers = this.headers;

        if(queries)
            queries.copy(this.queries);
        else
            queries = this.queries;

        if(resources)
            resources.copy(this.resources);
        else
            resources = this.resources;

        config.url = this.uri(resources);
        config.baseURL = this.url;
        config.headers = (headers ? headers.pairs : {});
        config.params = (queries ? queries.pairs : {});

        if(authentication)
            this.header('Authorization', authentication.authorization);


        return config;
    }

    public delete(...args): Promise<ServiceResponse<D>>{
        args.unshift(HttpMethod.DELETE);
        return this.streamless(args);
    }

    protected streamed(args: Array<any>): Promise<ServiceResponse<D>>{
        let argue = new Argumenter(args);
        let method = <HttpMethod> argue.number;
        let definable = argue.instance(Definable);
        
        let url = this.url;
        let type = this.type;
        let config = this.config;
        let data = ( definable ? json(json(definable)) : undefined );
        return new Promise<ServiceResponse<D>>((accept = () => {}, reject = () => {}) =>{
            let promise:AxiosPromise;
            switch(method){
                case HttpMethod.POST: 
                    promise = axios.post(url, data, config());
                    break;

                case HttpMethod.PATCH: 
                    promise = axios.patch(url, data, config());
                    break;

                case HttpMethod.PUT: 
                    promise = axios.put(url, data, config());
                    break;

                case HttpMethod.DELETE: 
                    reject(new AxiosFault(405, "Method Not Allowed"));
                    break;

                default:
                    reject(new AxiosFault(405, "Method Not Allowed"));
                    return;
            }
            promise.then( response => {
                let code = response.status;
                let headers = response.headers;
                let data = response.data;
                let list = new Array<D>();
                list.type = type;
                let name = this.name;
                if(data){
                    
                    let array:Array<Object> = data[name];

                    if(Array.isArray(array)){
                        array.forEach( item => {
                            let definable = new type();
                            copy(item, definable);
                            list.add(definable);
                        });
                    }
                    else{
                        let definable = new type();
                        copy(data, definable);
                        list.add(definable);
                    }
                        
                }

                accept({data: list, headers: new HeaderMap(headers), code: code})
            })
            .catch( response => {new AxiosFault(response)});
        })
    }

    protected streamless(args: Array<any>){
        let url = this.url;
        let type = this.type;
        let name = this.name || plural(type);

        let argue = new Argumenter(args);
        let method = argue.number;
        let filter = <D> argue.instance(Definable);
        let queries = argue.instance(QueryMap);
        let resources = argue.instance(ResourceMap);
        let headers = argue.instance(HeaderMap);
        
        if(filter){
            queries = new QueryMap(filter);
            resources = new ResourceMap(filter);
        }


        let config = this.config(queries, resources, headers);

        let promise: AxiosPromise;

        return new Promise<ServiceResponse<D>>((accept = () => {}, reject = () => {}) =>{
            try{
                switch(method){
                    case HttpMethod.GET: 
                        promise = axios.get(url, config);
                        break;
    
                    case HttpMethod.DELETE: 
                        promise = axios.delete(url, config);
                        break;
    
                    case HttpMethod.HEAD: 
                        promise = axios.head(url, config);
                        break;
    
                    default:
                        reject(new AxiosFault(405, "Method Not Allowed"));
                        return;
                }
                promise.then( response => {
                    let code = response.status;
                    let headers = response.headers;
                    let data = response.data;
                    let list = new Array<D>();
                    
                    list.type = type;
                    
                    if(data){
                        
                        let array:Array<Object> = (Array.isArray(data) ? data : data[name] );
    
                        if(Array.isArray(array)){
                            array.forEach( item => {
                                let definable = new type();
                                copy(item, definable);
                                list.add(definable);
                            });
                        }
                        else{
                            let definable = new type();
                            copy(data, definable);
                            list.add(definable);
                        }
                            
                    }
    
                    accept({code: code, headers: headers, data: list});
                })
                .catch( (error:AxiosError) => {
                    reject(new AxiosFault(error));
                })
            }
            catch(err){
                reject(new AxiosFault(err));
            }
        })
    }

}

function dataArray(argue:Argumenter): Array<Definable>{
    let array = new Array<Definable>();

    let data = argue.instance(Definable);
    while(data){
        array.push(data);
        data = argue.instance(Definable);
    }
    return array;
}