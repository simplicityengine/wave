import { Argumenter, json, dive } from "wave-core";
import {AxiosError} from "axios";

export class AxiosFault extends Error{
    /** error code */
    public code: number;

    /** error message */
    public message: string;
    
    public constructor();

    /** @param {number} code error code value*/
    public constructor(code: number);

    /** @param {string} message error message value*/
    public constructor(message: string);

    /** @param {number} code error code value
     *  @param {string} message error message value */
    public constructor(code: number, message: string);

    /** @param {number} code error code value
     *  @param {string} message error message value */
    public constructor(message: string, code: number);

    /** @param {AxiosError} error axios error */
    public constructor(error: AxiosError);

    /** @param {Error} error javascript error */
    public constructor(error: Error)

    /** @param {AxiosError} error axios error
     *  @param {string} path path to find error in the response body */
    public constructor(error: AxiosError, path: string)

    /** @param {Error} error javascript error
     *  @param {string} path path to find error in the response body */
    public constructor(error: Error, path: string)

    public constructor(...args){
        super();
        let argue = new Argumenter(args);
        this.code = argue.number || 500;
        this.message = argue.string || "Unknown";
        let err:AxiosError = <any> argue.object;
        
        if(err){
            let path = argue.string || 'message';
            this.message = err.message;
            this.stack = err.stack;
            let response = err.response;

            if(response){
                this.code = response.status || 500;
                this.message = response.statusText || err.message || 'Unknown';
                let body = response.data;

                if(typeof body === 'string'){
                    let object = json(body);
                    this.message = dive(object, path);
                }
                else if(typeof body === 'object'){
                    this.message = dive(body, path);
                }
            }
        }
    }
    
}