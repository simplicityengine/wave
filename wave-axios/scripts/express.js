const lib = require("../lib");
const utils = require("../tests/utils");
const data = require("../tests/data");
const resolve = require("path").resolve;
var express = require("express");



const app = express();
app.use('/docs', express.static(resolve('lib/documentation')));
app.get('/:action', (req, res) => {
    
    /**@type {string} */
    let action = req.params.action;
    let result = {};
    result[action] = data[action];
    res.header('Content-Type', 'application/json')
    res.send(JSON.stringify(result));
});


app.listen(8080, () => console.log('ExressJS listening on port 8080!'));