const fs = require("fs");
const {format, ok} = require("wave-core");
const {resolve, relative, sep} = require("path");
const {execSync} = require("child_process");
const package = require(resolve("package.json"));
var outputPath = resolve("lib/documentation");
var inputPath = resolve("src/index.ts");
var readmePath = resolve("README.md");
let options = {
    out: outputPath,
    name: package.name,
    mode: 'file',
    readme: (fs.existsSync(readmePath) ? readmePath : undefined)
}

function command(options) {
    let sb = "npm run typedoc -- ";

    for(var key in options){
        let value = options[key];
        if(ok(value)){
            sb += format(" --{key} {value}", {key, value})
        }
    }
    sb += format(" {}", inputPath);
    return sb;
}


if(fs.existsSync(outputPath)){
    execSync(format("rm -rf {}", outputPath))
}
console.info(execSync(command(options),{encoding: "utf8"}));