var resolve = require("path").resolve;
var fs = require("fs");
var classification = require("../../lib/classification");
var ASTClass = classification.ASTClass;
/**
 * @param {string} path
 */
function Unit (path){
    path = this.path = resolve(path);
    this.data = fs.readFileSync(path, "utf-8");
    let ast = this.ast = new ASTClass(path)

    Object.defineProperty(this, 'typescript', {enumerable: true, get: () => {return ast.typescript}});
    Object.defineProperty(this, 'javascript', {enumerable: true, get: () => {return ast.javascript}});
    Object.defineProperty(this, 'template', {enumerable: true, get: () => {return ast.template}});
    Object.defineProperty(this, 'style', {enumerable: true, get: () => {return ast.style}});
}

/** @type {string} */
Unit.prototype.path = undefined;

/** @type {string} */
Unit.prototype.typescript = undefined;

/** @type {string} */
Unit.prototype.javascript = undefined;

/** @type {string} */
Unit.prototype.template = undefined;

/** @type {string} */
Unit.prototype.style = undefined;

/** @returns {void} **/
Unit.prototype.load = function(){
    let ast = this.ast;
    ast.load(this.data)
}

/** @returns {void} **/
Unit.prototype.pitch = function(){
    let path = this.path;
    let ast = this.ast;
    ast.pitch(path);
}

exports.Unit = Unit