// import modules
var fs = require("fs");
var resolve = require("path").resolve;
var compiler = require("vue-template-compiler");
var classification = require("../lib/classification")
var globalization = require("../lib/globalization");
var utils = require("./utils")

//import types
var ASTClass = classification.ASTClass;
var Unit = utils.Unit;
var dependencies = globalization.dependencies;

let app = new Unit("tests/app.vue");
let table = new Unit("tests/table.vue")

app.pitch();
table.pitch();
app.load();
table.load();
console.log(table.typescript);