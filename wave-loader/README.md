# README #

Webpack loader for the wave framework

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

**01)** [nodejs](https://nodejs.org/en/)<br/>
**02)** [vscode](https://code.visualstudio.com/)<br/>
**03)** [typescript](https://www.npmjs.com/package/typescript)<br/>
**04)** [path](https://www.npmjs.com/package/path)<br/>
**05)** [parse5](https://www.npmjs.com/package/parse5)<br/>
**06)** [ts-simple-ast](https://www.npmjs.com/package/ts-simple-ast)<br/>
**07)** [vue-template-compiler](https://www.npmjs.com/package/vue-template-compiler)<br/>
**08)** [wave-core](https://npmregistry.simplicitylabs.ca/#/detail/wave-core)<br/>


### Installing

Do the following steps to install **wave-loader**:

**01)** Setup credentials to the **simplicityLabs** NPM registry
```
npm adduser --registry https://npmregistry.simplicitylabs.ca
```
**NOTE:** contact the [registry administrator](mailto:joseph@simplicitylabs.ca?Subject=NPM%20Registry%20Access) to get your [bitbucket](https://bitbucket.org) user account mapped to the [simplicityLabs registry](https://npmregistry.simplicitylabs.ca).  Use your [bitbucket](https://bitbucket.org) password when prompted.

**02)** Install the **wave-loader** npm package
```
npm install wave-loader --save --registry https://npmregistry.simplicitylabs.ca
```

## Publish

**01)** To tranpile the source into javascript, execute the following:<br/>
`npm run build`

**02)** To publish a patch on the [simplicityLabs registry](https://npmregistry.simplicitylabs.ca), execute the following:<br/>
`npm run patch`

## Built With

**01)** [wave-core](https://bitbucket.org/simplicityengine/wave) - Utilities Core<br/>
**02)** [vue-template-compiler](https://github.com/vuejs/vue) - Single File Component Compiler<br/>
**03)** [ts-simple-ast](https://github.com/dsherret/ts-simple-ast) - TypeScript Compiler Wrapper<br/>
**04)** [parse5](http://inikulin.github.io/parse5) - NodeJS Shim for Javascript DOMParser<br/>

## Authors

**01)** **Joseph Eniojukan** - *Initial work* - [joejukan](https://bitbucket.org/joejukan/)<br/>


## License

This project is licensed under the MIT License - see the **LICENSE.md** file for details
```
Copyright 2017 simplicityEngine

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
